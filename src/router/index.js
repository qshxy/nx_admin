import Vue from 'vue'
import Router from 'vue-router'
/* import HelloWorld from '@/components/HelloWorld'; */
import GoodsList from '@/views/goodsList.vue';
import Cart from '../views/cart.vue';
import Address from '../views/address.vue';
import orderConfirm from '../views/orderConfirm.vue'
import orderSuccess from '../views/orderSuccess.vue';
import orderList from '../views/orderList.vue';
import home from '../views/home.vue';
import main from '../pages/main.vue';
import eachart from '../pages/eachart.vue';
import table from '../pages/table.vue';
import markdown from '../pages/markdown.vue';
import resume from '../pages/resume.vue';
import register from '../pages/register.vue';
import searchRes from '../pages/searchRes.vue';
import newAdd from '../components/newAdd.vue'
Vue.use(Router)

export default new Router({
  routes: [{
      path: '/', ///:goodid/user/:name,传参数时用的
      name: 'Goods',
      component: GoodsList,
      hidden: true
    },
    {
      path: '/goods', ///:goodid/user/:name,传参数时用的
      name: 'Goods',
      component: GoodsList,
      hidden: true
    },
    {
      path: '/goods',
      name: 'GoodsList',
      component: GoodsList,
      hidden: true
    },
    {
      path: '/cart', // /:id
      name: "cart",
      component: Cart,
      hidden: true
    },
    {
      path: '/address',
      name: 'address',
      component: Address,
      hidden: true
    },
    {
      path: '/orderConfirm',
      name: 'orderConfirm',
      component: orderConfirm,
      hidden: true
    }, {
      path: '/orderSuccess',
      name: 'orderSuccess',
      component: orderSuccess,
      hidden: true
    }, {
      path: '/orderList',
      name: 'orderList',
      component: orderList,
      hidden: true
    },

    {
      path: '/searchRes',
      name: 'searchRes',
      component: searchRes,
      hidden: true
    },
    {
      path: '/newAdd',
      name: 'newAdd',
      component: newAdd,
      hidden: true
    },
    {
      path: '/home',
      component: home,
      name: '',
      nav: '',
      iconCls: 'el-iconfont-erp-zhuye',
      leaf: true, //只有一个节点
      redirect: '/home/main',
      children: [{
        path: '/home/main',
        component: main,
        name: '首页'
      }]
    }, {
      path: '/home',
      name: '首页',
      component: home,
      nav: '图表',
      iconCls: 'el-iconfont-erp-biaoge',
      children: [
        /* {
                path: '/home/',
                redirect: '/home/resume',
                component: resume,
                name: resume,
                children: [{
                  path: '/home/resume'
                }]
              }, */
        {
          path: '/home/eachart',
          component: eachart,
          name: '折线图'
        }, {
          path: '/home/register',
          component: register,
          name: '地图注册'
        }, {
          path: '/home/resume',
          component: resume,
          name: '关于我们',
          /*   hidden:true */
          /* children:[
            {
              path: '/home/table',
              component: table,
              name: '订单地址'
            }
          ] */
        }
      ]
    },
    {
      path: '/home',
      name: '首页',
      component: home,
      nav: '',
      leaf: true,
      iconCls: 'el-iconfont-erp-mail',
      children: [{
        path: '/home/table',
        component: table,
        name: '订单地址'
      }]
    }, {
      path: '/home',
      name: '首页',
      component: home,
      nav: '编辑器',
      iconCls: 'el-iconfont-erp-link',
      children: [{
        path: '/home/markdown',
        component: markdown,
        name: '编辑器'
      }, ]
    }
  ]
})
