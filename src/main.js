// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router';
//图片懒加载
import VueLazyload from 'vue-lazyload';
Vue.use(VueLazyload,{
     preLoad: 1.3,
    // error: 'dist/error.png',
     loading: '../static/loading-svg/loading-spin.svg',
    // attempt: 1
});
 //滚动加载更多
import infiniteScroll from 'vue-infinite-scroll';
Vue.use(infiniteScroll);
import Axios from 'axios';
import common from './common/js/common.js';
//在vue上挂载一个可用原型
Vue.prototype.$axios = Axios;
Vue.prototype.common = common;
Vue.config.productionTip = false
//定义全局价格过滤器
import {currency} from './util/currency';
Vue.filter('currency', currency)
//cssd的引入
import './assets/css/base.css';
import './assets/css/checkout.css';
import './assets/css/login.css';
import './assets/css/product.css';
import './assets/iconfont/iconfont.css';
/* import './assets/css/style.css'; */
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import VCharts from 'v-charts';
import VueQuillEditor from 'vue-quill-editor';

Vue.use(ElementUI);
Vue.use(VCharts);
// require styles
//编辑器
Vue.use(VueQuillEditor)

//引入vuex
import store from './store/store'
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
  //render: h => h(App)
})
