import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

// 建立store对象
const store = new Vuex.Store({
  state: {
    nickName: '', // 用户名
    cartCount: 0 ,// 购物车数量
    searchName:''
  },
  mutations: { // 更改状态
    //更新用户信息
    updateUserInfo(state, nickName) {//用户名
      state.nickName = nickName;
    },
     updatesearchName(state, searchName) { //用户名
       state.searchName = searchName;
       //将searchName存储到localStorage
         localStorage.setItem('searchName', JSON.stringify(state.searchName))
     },
    updateCartCount(state, cartCount) {//购物车总数量
      state.cartCount += cartCount;
    },
    initCartCount(state, cartCount) {
      state.cartCount = cartCount;

    }
  }
});
export default store;
