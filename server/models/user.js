var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var userSchema = new Schema({
  "userId": String,
  "userName": String,
  "userPwd": String,
  "orderList": Array,
  "creatTime": String,
  "sex": String,
  "cartList": [{
    "productImage": String,
    "salePrice": String,
    "productName": String,
    "productId": String,
    "productNum": String,
    "checked": String,
  }],
  "addressList": [{
      "addressId": String,
      "userName": "String",
      "streetName": "String",
      "postCode": Number,
      "tel": Number,
      "isDefault": Boolean
    }

  ]

})
module.exports = mongoose.model("User", userSchema)
