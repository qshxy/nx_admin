  /* model实现层 */
var mongoose=require('mongoose');
//定义表模型
var Schema=mongoose.Schema;
//通过new一个Schema来定义一下数据
var productSchema=new Schema({
  //数据库字段
  "productId":String,
  "productName": String,
  "salePrice": Number,
  "productImage": String,
  "productUrl": String,
   "productNum": String,
    "checked": String,
})
//输出文件5.将该Schema发布为Model
module.exports = mongoose.model('Good', productSchema);
//输出就可以基于api调用方法

