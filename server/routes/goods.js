/* 二级路由 */
/* 通过express对象拿到当前的路由 */
var express = require('express');
/* 拿到express的路由 */
var router = express.Router();
/* 获取mongoose */
var mongoose = require('mongoose');
/* 加载模型表 */
var Goods = require('../models/goods');
var User = require('./../models/user');
/* 连接数据库 */
mongoose.connect('mongodb://127.0.0.1:27017/test');
mongoose.connection.on("connected", function () {
  console.log("连接成功")
});
mongoose.connection.on("error", function () {
  console.log("连接失败")
});
mongoose.connection.on("disconnected", function () {
  console.log("连接断开")
});
/* 实现路由 */
/* 二级路由通过router.get */
//查询商品列表
router.get('/list', function (req, res, next) {
  //分页参数第几页
  let page = parseInt(req.param('page'));
  //一页多少条数据
  let pageSize = parseInt(req.param('pageSize'));
  //排序
  //通过param获取前端参数
  let sort = parseInt(req.param('sort'));
  //分页公式,skip索引值
  let skip = (page - 1) * pageSize;
  //价格区间定义
  var priceGt = '',
    priceLte = '';
  //定义一个参数对象
  let params = {};
  //接收价格排序的字段
  let priceLevel = req.param('priceLevel');
  //价格过滤判断
  if (priceLevel != 'all') {
    switch (priceLevel) {
      case '0':
        priceGt = 0;
        priceLte = 100;
        break;
      case '1':
        priceGt = 100;
        priceLte = 500;
        break;
      case '2':
        priceGt = 500;
        priceLte = 1000;
        break;
      case '3':
        priceGt = 1000;
        priceLte = 5000;
        break;
      case '4':
        priceGt = 5000;
        priceLte = 10000;
        break;
      case '5':
        priceGt = 10000;
        priceLte = 9999999999;
        break;
    }
    //重新定义，包含了要
    params = {
      salePrice: {
        //参数用于下面数据库查询时用到，$gt是大于,$lte是小于
        $gt: priceGt,
        $lte: priceLte
      }
    }
  } else {
    switch (priceLevel) {
      case 'all':
        priceGt = 0;
        priceLte = 9999999999;
        break;
    }
    //重新定义，包含了要
    params = {
      salePrice: {
        //参数用于下面数据库查询时用到，$gt是大于,$lte是小于
        $gt: priceGt,
        $lte: priceLte
      }
    }
  }

  //find可以返回一个模型,skip()方法来跳过指定数量的数据
  //limit()方法接受一个数字参数，该参数指定从MongoDB中读取的记录条数。
  //params用于条件查询
  let goodsModel = Goods.find(params).skip(skip).limit(pageSize);
  //调用sort（）方法进行排序,sort=1是升序，-1是降序，根据价格来排序
  goodsModel.sort({
    "salePrice": sort
  })

  //Goods数据模型查询
  goodsModel.exec(function (err, doc) {
    //doc是mongodb中的数据
    if (err) {
      /* 失败的函数 */
      res.json({
        status: "1",
        msg: err.message
      })
    } else {
      /* 成功函数 */
      res.json({
        /* 定义成功请求的json格式 */
        status: "0",
        msg: "",
        result: {
          count: doc.length,
          list: doc

        }
      })
    }
  })
})
//加入到购物车
router.post('/addCart', function (req, res, next) {
  var userId = req.cookies.userId;
  //post方法去参数是用req.body
  var productId = req.body.productId;
  //通过模型来查数据
  User.findOne({
    userId: userId
  }, function (err, userDoc) {
    if (err) {
      /* 失败的函数 */
      res.json({
        status: "1",
        msg: err.message
      })
    } else {
      console.log(userDoc);
      if (userDoc) {
        let goodsItem = "";
        userDoc.cartList.forEach(item => {
          if (item.productId == productId) {
            goodsItem = item;
            item.productNum++;
          }
        });
        if (goodsItem) {
          //如果已经加入购物车的只需要数量加加
          userDoc.save(function (err2, doc2) {
            if (err2) {
              res.json({
                status: "1",
                msg: err2.message
              })
            } else {
              res.json({
                status: "0",
                msg: "",
                result: "suc"
              })
            }
          })

        } else {
          /* 如果购物车中没有需要重新加入 */
          Goods.findOne({
            productId: productId
          }, function (err1, doc) {
            //新建一个对象存储查询到的数据，并且添加新的属性
            var newObj = null;
            if (err1) {
              /* 失败的函数 */
              res.json({
                status: "1",
                msg: err1.message
              })
            } else {
              if (doc) {

                //添加山字段
                // doc.productNum = "1";
                //doc.checked ="1";
                newobj = { //新创建一个对象，实现转换mongoose不能直接增加属性的坑
                  productNum: "1",
                  checked: "1",
                  productId: doc.productId,
                  producName: doc.producName,
                  salePrice: doc.salePrice,
                  productName: doc.productName,
                  productImage: doc.productImage,
                }
                /* userDoc已经是数据了 */
                userDoc.cartList.push(newobj);
                userDoc.save(function (err2, doc2) {
                  if (err2) {
                    res.json({
                      status: "1",
                      msg: err2.message
                    })
                  } else {
                    res.json({
                      status: "0",
                      msg: "",
                      result: "suc"
                    })
                  }
                })
              }
            }
          });
        }
      }
    }
  })
});
//添加商品
router.post('/addGoods', function (req, res, next) {
  var productName = req.body.productName,
    salePrice = req.body.salePrice,
    productImage = req.body.productImage,
    productId = req.body.productId;
  var newGoods = new Goods({
    "productId": productId,
    "productName": productName,
    "salePrice": salePrice,
    "productImage": productImage,
    "productUrl": ""
  });
  Goods.findOne({
    productId: productId
  }, function (err, goodsDoc) {
    if (err) {
      res.json({
        status: '1',
        msg: err.msg,
        result: ''
      })
    } else {
      if (goodsDoc) {
        res.json({
          status: '1102',
          msg: '商品已存在',
          result: ''
        })
      } else {
        newGoods.save(function (err, doc) {
          if (err) {
            res.json({
              status: '1',
              msg: err.message,
              result: ''
            })
          } else {
            res.json({
              status: '0',
              msg: '',
              result: 'suc'
            })
          }
        })
      }
    }
  })
});
//删除商品
router.post('/delGoods', function (req, res, next) {
  var productId = req.body.productId;
  Goods.remove({
    'productId': productId
  }, function (err, doc) {
    if (err) {
      res.json({
        status: '1',
        msg: err.message,
        result: ''
      })
    } else {
      res.json({
        status: '0',
        msg: '',
        result: 'suc'
      })
    }
  })
});
//修改产品
router.post('/updateGoods', function (req, res, next) {
  var productId = req.body.productId,
    productName = req.body.productName,
    salePrice = req.body.salePrice,
    productImage = req.body.productImage;
  Goods.update({
    "productId": productId
  }, {
    'productName': productName,
    'salePrice': salePrice,
    "productImage": productImage
  }, function (err, doc) {
    if (err) {
      res.json({
        status: '1',
        msg: err.message,
        result: ''
      })
    } else {
      res.json({
        status: '0',
        msg: '',
        result: {
          'productName': doc.productName,
          'salePrice': doc.salePrice,
          'productImage': doc.productImage,
        }
      })
    }
  })
});
//修改时弹出框获取详情
router.get('/goodsDetails', function (req, res, next) {
  var productId = req.param('productId');
  Goods.findOne({
    'productId': productId
  }, function (err, doc) {
    if (err) {
      res.json({
        status: '0',
        msg: err.message,
        result: ''
      })
    } else {
      res.json({
        status: '0',
        msg: '',
        result: {
          productName: doc.productName,
          salePrice: doc.salePrice,
          productImage: doc.productImage,
        }
      })
    }
  })
})
/* 输出路由 */
module.exports = router;
/* axios不支持跨域，需要做一个代理插件不用跨域了，在config下的index.js中的proxyTable：{
   '/goods': {
     target: 'http://localhost:3000'
     //将3000端口以前端渲染的端口输出
   }
} */
