var express = require("express");
var router = express.Router();
var User = require("./../models/user");
//创建时间方法
require('./../util/util')
/* GET users listing. */
router.get("/", function (req, res, next) {
  res.send("respond with a resource");
});
router.post("/login", function (req, res, next) {
  //去参数
  var param = {
    userName: req.body.userName,
    userPwd: req.body.userPwd
  };
  User.findOne(param, function (err, doc) {
    if (err) {
      res.json({
        status: "1",
        msg: err.message,
        result: ''
      });
    } else {
      if (doc) {
        //保存cookie来保存数据第一个参数为名称，第二个为传进去的值，第三个为设置项
        res.cookie("userId", doc.userId, {
          path: "/",
          maxAge: 1000 * 60 * 60
        });
        res.cookie("userName", doc.userName, {
          path: "/",
          maxAge: 1000 * 60 * 60
        });
        //session是通过发请求得到的所以用req接收
        //req.session.user=doc;
        res.json({
          status: "0",
          msg: "",
          result: {
            userName: doc.userName,
            userPwd: doc.userPwd
          }
        });
      } else {
        res.json({
          status: "100101",
          msg: "用户不存在",
          result: ''
        });
      }
    }
  });
});
//退出
router.post("/logout", function (req, res, next) {
  //清除cookie
  res.cookie("userId", "", {
    path: "/",
    maxAge: -1
  });
  res.json({
    status: "0",
    msg: "",
    result: ""
  });
});
//登录校验，刷新显示用户
router.get("/checkLogin", function (req, res, next) {
  if (req.cookies.userId) {
    res.json({
      status: "0",
      msg: "",
      result: req.cookies.userName || ""
    });
  } else {
    res.json({
      status: "1",
      msg: "未登录",
      result: ""
    });
  }
});
//查看购物车总数量
router.get('/getCartCount', function (req, res, next) {
  //判断是否存在cookies
  if (req.cookies && req.cookies.userId) {
    var userId = req.cookies.userId;
    User.findOne({
      "userId": userId
    }, function (err, doc) {
      if (err) {
        res.json({
          status: '1',
          msg: err.message,
          result: ''
        });
      } else {
        var cartList = doc.cartList;
        var cartCount = 0;
        cartList.map(function (item) {
          cartCount += parseFloat(item.productNum);
        });
        res.json({
          status: '0',
          msg: '',
          result: cartCount
        })
      }
    });
  } else {
    res.json({
      status: '10101',
      msg: '用户不存在',
      result: ''
    })
  }
})
//查询当前用户购物车的数据
router.get("/cartList", function (req, res, next) {
  let userId = req.cookies.userId;
  User.findOne({
      userId: userId
    },
    function (err, doc) {
      if (err) {
        res.json({
          status: "1",
          msg: err.message,
          result: ""
        });
      } else {
        res.json({
          status: "0",
          msg: "",
          result: doc.cartList
        });
      }
    }
  );
});
//删除购物车数据
router.post("/cartDel", function (req, res, next) {
  var userId = req.cookies.userId;
  var productId = req.body.productId;
  //mongodb可以通过update和$pull删除数据
  User.update({
      userId: userId
    }, {
      $pull: {
        //删除元素
        cartList: {
          productId: productId
        }
      }
    },
    function (err, doc) {
      if (err) {
        res.json({
          status: "1",
          msg: err.message,
          result: ""
        });
      } else {
        res.json({
          status: "0",
          msg: "",
          result: "suc"
        });
      }
    }
  );
});
//购物车中数量加减的变化接口
router.post("/cartEdit", function (req, res, nest) {
  //获取请求的参数
  var userId = req.cookies.userId,
    productId = req.body.productId,
    productNum = req.body.productNum,
    checked = req.body.checked;
  //更新数据库数据第一个参数是更新查找到的数据的条件，第二个参数是需要更新的数据
  User.update({
      userId: userId,
      "cartList.productId": productId
    }, {
      "cartList.$.productNum": productNum,
      "cartList.$.checked": checked
    },
    function (err, doc) {
      if (err) {
        res.json({
          status: "1",
          msg: err.message,
          result: ""
        });
      } else {
        res.json({
          status: "0",
          msg: "",
          result: "suc"
        });
      }
    }
  );
});
//全选和取消全选
router.post("/editCheckAll", function (req, res, next) {
  var userId = req.cookies.userId,
    checkAll = req.body.checkAll ? "1" : "0";
  User.findOne({
    userId: userId
  }, function (err, user) {
    if (err) {
      res.json({
        status: "1",
        msg: err.message,
        result: ""
      });
    } else {
      if (user) {
        user.cartList.forEach((item) => {
          item.checked = checkAll;
        })
        user.save(function (err1, doc) {
          if (err1) {
            res.json({
              status: '1',
              msg: err1.message,
              result: ''
            })
          } else {
            res.json({
              status: '0',
              msg: '',
              result: 'suc'
            })
          }
        })
      }
    }
  });
});
//查询用户地址列表接口
router.get('/addressList', function (req, res, next) {
  var userId = req.cookies.userId;
  User.findOne({
    userId: userId
  }, function (err, doc) {
    if (err) {
      res.json({
        status: '1',
        msg: err.message,
        result: ''
      })
    } else {
      res.json({
        status: '0',
        msg: '',
        result: doc.addressList
      })
    }
  })
});
//设置默认地址
router.post('/setDefault', function (req, res, next) {
  var userId = req.cookies.userId,
    addressId = req.body.addressId;
  if (!addressId) {
    res.json({
      status: '100322',
      msg: '设置失败',
      result: ''
    })
  } else {
    User.findOne({
      userId: userId
    }, function (err, doc) {
      if (err) {
        res.json({
          status: '1',
          msg: err.message,
          result: ''
        })
      } else {
        var addressList = doc.addressList;
        addressList.forEach((item) => {
          if (item.addressId == addressId) {
            item.isDefault = true;
          } else {
            item.isDefault = false;
          }
        });
        doc.save(function (err1, doc1) {
          if (err1) {
            res.json({
              status: '1',
              msg: err.message,
              result: ''
            })
          } else {
            res.json({
              status: '0',
              msg: '',
              result: 'suc'
            })
          }
        })
      }
    })
  }

});
//删除地址接口
router.post("/delAddress", function (req, res, next) {
  var userId = req.cookies.userId;
  var addressId = req.body.addressId;
  //mongodb可以通过update和$pull删除数据
  User.update({
      userId: userId
    }, {
      $pull: {
        //删除元素
        addressList: {
          addressId: addressId
        }
      }
    },
    function (err, doc) {
      if (err) {
        res.json({
          status: "1",
          msg: err.message,
          result: ""
        });
      } else {
        res.json({
          status: "0",
          msg: "",
          result: "suc"
        });
      }
    }
  );
});
//删除订单列表中的数据
router.post('/delOrderList', function (req, res, next) {
  var userId = req.cookies.userId;
  var orderId = req.body.orderId;
  User.update({
    userId: userId
  }, {
    $pull: {
      orderList: {
        orderId: orderId
      }
    }
  }, function (err, doc) {
    if (err) {
      res.json({
        status: "1",
        msg: err.message,
        result: ""
      });
    } else {
      res.json({
        status: "0",
        msg: "",
        result: "suc"
      });
    }
  })
})
//添加地址
router.post('/addAddress', function (req, res, next) {
  var userId = req.cookies.userId,
    addressId = req.body.addressId,
    userName = req.body.userName,
    streetName = req.body.streetName,
    tel = req.body.tel
  User.findOne({
    userId: userId
  }, function (err, doc) {
    var newObj = null;
    if (err) {
      /* 失败的函数 */
      res.json({
        status: "1",
        msg: err.message
      })
    } else {
      if (doc) {
        newobj = { //新创建一个对象，实现转换mongoose不能直接增加属性的坑
          addressId: addressId,
          userName: userName,
          streetName: streetName,
          postCode: addressId,
          tel: tel,
          isDefault: false,
        }
        doc.addressList.push(newobj);
        doc.save(function (err2, doc2) {
          if (err2) {
            res.json({
              status: "1",
              msg: err2.message
            })
          } else {
            res.json({
              status: "0",
              msg: "",
              result: "suc"
            })
          }
        })
      }
    }
  })
});
//支付接口
router.post('/payMent', function (req, res, next) {
  var userId = req.cookies.userId,
    orderTotal = req.body.orderTotal,
    addressId = req.body.addressId
  User.findOne({
    userId: userId
  }, function (err, doc) {
    if (err) {
      res.json({
        status: '1',
        msg: err.message,
        result: ''
      })
    } else {
      var address = '',
        goodsList = []; //保存循环遍历出来的地址
      //获取当前用户的地址
      doc.addressList.forEach((item) => {
        if (item.addressId == addressId) {
          address = item;
        }
      });
      //获取用户购物车的商品
      doc.cartList.filter((item) => {
        if (item.checked == '1') {
          //用户需要购买的商品
          goodsList.push(item)
        }
      });
      //创建订单id
      var platnum = '100'
      var r1 = Math.floor(Math.random() * 10);
      var r2 = Math.floor(Math.random() * 10);
      var sysDate = new Date().Format('yyyyMMddhhmmss');
      var orderId = platnum + r1 + sysDate + r2;
      //创建日期时间
      var createDate = new Date().Format('yyyy-MM-dd hh:mm:ss')
      var order = {
        orderId: orderId,
        orderTotal: orderTotal,
        addressInfo: address,
        goodsList: goodsList,
        orderStatus: "1",
        createDate: createDate
      };
      doc.orderList.push(order);
      doc.save(function (err1, doc1) {
        if (err1) {
          res.json({
            status: '1',
            msg: err1.message,
            result: ''
          })
        } else {
          res.json({
            status: '0',
            msg: '',
            result: {
              //返回的值以便能查询到
              orderId: order.orderId,
              orderTotal: order.orderTotal
            }
          })
        }
      })
    }
  })
});
//订单成功页面，根据订单id查询
router.get('/orderDetail', function (req, res, next) {
  var userId = req.cookies.userId,
    orderId = req.param('orderId')
  User.findOne({
    userId: userId
  }, function (err, userInfo) {
    if (err) {
      res.json({
        status: '1',
        msg: err.message,
        result: ''
      })
    } else {
      var orderList = userInfo.orderList; //订单列表
      //判断是否有订单
      if (orderList.length > 0) {
        var orderTotal = 0,
          addressInfo = '';
        //循环遍历查找存在的订单
        orderList.forEach((item) => {
          if (item.orderId == orderId) {
            createDate = item.createDate;
            orderTotal = item.orderTotal;
            addressInfo = item.addressInfo;
            goodsList = item.goodsList;
          }
        });
        if (orderTotal > 0) {
          res.json({
            status: '0',
            msg: '',
            result: {
              createDate: createDate,
              orderId: orderId,
              orderTotal: orderTotal,
              addressInfo: addressInfo,
              goodsList: goodsList
            }
          })
        } else {
          res.json({
            status: '120001',
            msg: '无此订单',
            result: ''
          })
        }
      } else {
        res.json({
          status: '120002',
          msg: '当前用户没有创建订单',
          result: ''
        })
      }
    }
  })
});
//注册
router.post('/registe', function (req, res, next) {
  var zuserName = req.body.zuserName;
  var zuserPwd = req.body.zuserPwd;
  var ztime = req.body.ztime;
  var sex = req.body.sex;
  //创建userId
  var platnum = '100000'
  var r1 = Math.floor(Math.random() * 10);
  var r2 = Math.floor(Math.random() * 10);
  var r3 = Math.floor(Math.random() * 10);
  //var sysDate = new Date().Format('yyyyMMddhhmmss');
  var userId = platnum + r1 + r2 +r3;
  //创建新用户
  var user = new User({
    'userId': userId,
    'userName': zuserName,
    'userPwd': zuserPwd,
    'creatTime': ztime,
    'sex': sex,
    'orderList': [],
    'cartList': [],
    'addressList': []
  });
  User.findOne({
    userName: user.userName
  }, function (err, userDoc) {
    if (err) {
      res.json({
        status: '1',
        msg: err.message,
        result: ''
      });
    } else {
      if (userDoc) {
        res.json({
          status: '110',
          msg: '用户已存在',
          result: ''
        })
      } else {
        user.save(function (err, doc) {
          if (err) {
            res.json({
              status: '1',
              msg: err.message,
              result: ''
            })
          } else {
            res.json({
              status: '0',
              msg: '',
              result: {
                userName: doc.userName,
                userId: doc.userId
              }
            })
          }
        })
      }
    }
  })

});
//获取订单列表
router.get('/orderList', function (req, res, next) {
  var userId = req.cookies.userId;
  var userName = req.cookies.userName;
  //分页参数第几页
  /* let page = parseInt(req.param('page'));
  //一页多少条数据
  let pageSize = parseInt(req.param('pageSize'));
  let skip = (page - 1) * pageSize; */
  User.findOne({
    userId: userId
  }, function (err, doc) {
    if (err) {
      res.json({
        status: '1',
        msg: err.message,
        result: ''
      })
    } else {
      res.json({
        status: '0',
        msg: '',
        result: doc.orderList
      })
    }
  })
});
//查询列表
router.get('/search', function (req, res, next) {
  var userId = req.cookies.userId;
  var userName = req.cookies.userName;
  var name = req.param('name');
  //分页参数第几页
  /* let page = parseInt(req.param('page'));
  //一页多少条数据
  let pageSize = parseInt(req.param('pageSize'));
  let skip = (page - 1) * pageSize; */
  User.findOne({
      userId: userId
    },
    function (err, doc) {
      if (err) {
        res.json({
          status: '1',
          msg: err.message,
          result: ''
        })
      } else {
        var orderList = doc.orderList;
        var searchList = [];
        orderList.forEach((item) => {
          if (item.addressInfo.userName == name) {
            searchList.push(item);
          }
        });
        doc.save(function (err, doc1) {
          if (err) {
            res.json({
              status: '1',
              msg: eer.message,
              result: '',
            })
          } else {
            res.json({
              status: '0',
              msg: '',
              result: {
                'searchList': searchList
              }
            })
          }
        })

      }
    })
});
//批量删除接口
router.post('/deleteRow', function (req, res, next) {
  var userId = req.cookies.userId;
  var orderId = req.body.delarr;
  //console.log(orderId)
  User.update({
    userId: userId
  }, {
    '$pull': {
      'orderList': {
        'orderId': {
          '$in': orderId //包含在数组中。
        }
      }
    }
  }, function (err, doc) {
    if (err) {
      res.json({
        status: "1",
        msg: err.message,
        result: ""
      });
    } else {
      res.json({
        status: "0",
        msg: "",
        result: "suc"
      });
    }
  })
});
//修改密码
router.post('/changePwd', function (req, res, next) {
  var userId = req.cookies.userId,
    userPwd = req.body.userPwd;
  User.update({
    userId: userId
  }, {
    'userPwd': userPwd
  }, function (err, doc) {
    if (err) {
      res.json({
        status: '1',
        msg: err.message,
        result: ''
      })
    } else(
      res.json({
        status: '0',
        msg: '',
        result: 'suc'
      })
    )
  })
});
//查找有的用户
router.get('/userList', function (req, res, next) {
  User.find({}, function (err, doc) {
    if (err) {
      res.json({
        status: '1',
        msg: err.message,
        result: ''
      })
    } else {
      res.json({
        status: '0',
        msg: '',
        result: doc
      })
    }
  })
});
//删除用户
router.post('/delUsers', function (req, res, next) {
  var userId = req.body.userId;
  User.remove({
    userId: {
      $in: userId
    }
  }, function (err, doc) {
    if (err) {
      res.json({
        status: '1',
        msg: err.message,
        result: ''
      })
    } else {
      res.json({
        status: '0',
        msg: '',
        result: 'suc'
      })
    }
  })
});
//修改用户
router.post('/updateUsers', function (req, res, next) {
  var userId = req.body.userId,
    userName = req.body.userName,
    creatTime = req.body.creatTime,
    sex = req.body.sex;
  User.update({
    "userId": userId
  }, {
    'userName': userName,
    'creatTime': creatTime,
    "sex": sex
  }, function (err, doc) {
    if (err) {
      res.json({
        status: '1',
        msg: err.message,
        result: ''
      })
    } else {
      res.json({
        status: '0',
        msg: '',
        result: 'suc'
      })
    }
  })
});
module.exports = router;
